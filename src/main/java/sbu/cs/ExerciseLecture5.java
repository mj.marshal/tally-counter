package sbu.cs;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {

        String letters = "abcdefghijklmnopqrstuvxyz";
        StringBuilder ans = new StringBuilder();
        for( int i = 0; i < length; i++ ){
            int x = (int) (letters.length() * Math.random());
            ans.append( letters.charAt(x) );
        }
        return ans.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {

        String letters = "abcdefghijklmnopqrstuvxyz";
        String anyChar =   "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                            + "0123456789"
                            + "abcdefghijklmnopqrstuvxyz"
                            + "@_!*&$%";
        String numbers = "0123456789";
        String sc = "@_!*&$%";
        int xNum = (int) ( length * Math.random() );
        int xSC = (int) ( length * Math.random() );
        if( xNum == xSC )
            xSC = 0;
        if( xNum == xSC )
            xSC = length - 1;
        StringBuilder ans = new StringBuilder();
        boolean c = false;
        for( int i = 0; i < length; i++ ){
            if( i == xNum ) {
                int x = (int) (numbers.length() * Math.random());
                ans.append(numbers.charAt(x));
            }
            else if( i == xSC) {
                int x = (int) (sc.length() * Math.random());
                ans.append(sc.charAt(x));
            }
            else {
                if ( c ){
                    int x = (int) (anyChar.length() * Math.random());
                    ans.append(anyChar.charAt(x));
                }
                else {
                    int x = (int) (letters.length() * Math.random());
                    ans.append(letters.charAt(x));
                    c = true;
                }
            }
        }
        if( length < 3 ){
            throw new IllegalValueException();
        }
        return ans.toString();
    }


    private int bin( int x ) {

        int c = 0;
        while( x > 0 ){
            c += x & 1;
            x >>= 1;
        }
        return c;
    }

    private int fib( int x ) {

        if( x <= 1 ){
            return x;
        }
        return fib(x - 1 ) + fib(x - 2 );
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int len = (int) (Math.log(n) + 10);
        for(int i = 0; i < Math.min(len,n); i++ ){
            int x = fib(i);
            if( x + bin(x) == n )
                return true;
        }
        return false;
    }
}
