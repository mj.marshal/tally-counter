package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int x = 1;
        for (int i = 2; i <= n; i++ ) {
            x *= i;
        }
        return x;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {

        if( n <= 1 ) {
            return n;
        }
        return fibonacci (n-1) + fibonacci(n-2);
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {

        StringBuilder ans = new StringBuilder();
        ans.append(word);
        ans.reverse();
        return ans.toString();

    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {

        String lowerCase = line.toLowerCase(Locale.ROOT);
        StringBuilder sb = new StringBuilder();
        for( int i = 0; i < lowerCase.length(); i++ ){
            if( lowerCase.charAt(i) != ' ' ){
                sb.append(lowerCase.charAt(i));
            }
        }
        String str = sb.toString();
        int i = 0;
        int j = str.length() - 1;
        while( i < j ){
            if( str.charAt(i) != str.charAt(j) ) {
                return false;
            }
            i++;
            j--;
        }
        return true;

    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[][] ans = new char[str1.length()][str2.length()];
        for (int i = 0; i < str1.length(); i++){
            for (int j = 0; j < str2.length(); j++){
                if ( str1.charAt(i) == str2.charAt(j) ){
                    ans[i][j] = '*';
                }
                else {
                    ans[i][j] = ' ';
                }
            }
        }
        return ans;
    }
}
