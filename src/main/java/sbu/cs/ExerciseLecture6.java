package sbu.cs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long ans = 0;
        for( int i = 0; i < arr.length; i += 2 ){
            ans += arr[i];
        }
        return ans;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int size = arr.length;
        int[] ans = new int[size];
        for( int i = size - 1; i >= 0; i-- ){
            ans[size - 1 - i ] = arr[i];
        }
        return ans;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int r1 = m1.length;
        int c1 = m1[0].length;
        int c2 = m2[0].length;
        double[][] product = new double[r1][c2];
        for( int i = 0; i < r1; i++ ){
            for( int j = 0; j < c2; j++ ){
                for( int k = 0; k < c1; k++ ){
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }

        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> ans = new ArrayList<>();
        for( int i = 0 ; i < names.length; i++ ){
            ans.add(new ArrayList<>());
            for( int j = 0; j < names[0].length; j++ ){
                ans.get(i).add(names[i][j]);
            }
        }
        return ans;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {

        List<Integer> ans = new ArrayList<>();
        if ( n % 2 == 0)
            ans.add(2);
        while( n % 2 == 0 )
            n /= 2;
        for (int i = 3; i <= Math.sqrt(n); i+= 2) {
            if( n % i == 0 )
                ans.add(i);
            while( n % i == 0 )
                n /= i;
        }
        if (n > 2)
            ans.add(n);
        return ans;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] words = line.split("[^a-zA-Z]");
        List<String> ans = new ArrayList<>();
        Collections.addAll(ans, words);
        for( int i = 0; i < ans.size(); i++ ){
            if( ans.get(i).equals("") )
                ans.remove(i);
        }
        return ans;
    }
}
