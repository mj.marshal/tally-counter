package sbu.cs;

public interface TallyCounterInterface {

    void count();
    int getValue();
    void setValue(int value);
}
